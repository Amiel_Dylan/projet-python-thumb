import os, sqlite3
from pathlib import Path 
from flask import jsonify, send_from_directory
from Tools import utilities

def create_image_infos_table():
    """Method used to create an table for images informations

    Table details:
        images(id INTEGER PRIMARY KEY, width INTEGER, height INTEGER, weight INTEGER, format TEXT)

    Returns:
        [dict]: [return a dictionnary with an message if everything is okay, error message otherwhise]
    """
    #define connection and cursor
    connection = sqlite3.connect('Database\\thumbnailing_system.db') #connection

    cursor = connection.cursor() #cursor
    result = {}

    #create images information table
    try:
        create_table = """CREATE TABLE IF NOT EXISTS
        images(id INTEGER PRIMARY KEY, filename TEXT, width INTEGER, height INTEGER, weight INTEGER, format TEXT)"""
        cursor.execute(create_table)
        
        result["Message"] = "Table created successfully ✔️"

    finally:
        cursor.close()
    
    return result

def add_image_informations(image_informations):
    """Method used to add an image informations in the images table

    Args:
        image_informations (tuple): (filename: string, width :int, height :int, weight :int, format :string)

    Returns:
        [dict]: [return a dictionnary with the message and the image id if everything is okay, error message otherwhise]
    """
    #define connection and cursor
    connection = sqlite3.connect('Database\\thumbnailing_system.db') #connection

    cursor = connection.cursor() #cursor
    result = {}

    #insert image informations in the table
    try:
        query = "INSERT INTO images (filename,width,height,weight,format) VALUES (?,?,?,?,?)"
        
        cursor.execute(query, image_informations)
        
        connection.commit() # Save (commit) the changes

        cursor.execute('SELECT MAX(id) from images') # Trying to get the id of the last image added

        res = cursor.fetchone() # Reading the result

        result["image id"] = res[0] # We add the id of the last image added in the message

    except sqlite3.OperationalError as e:
        # Error message
        result["Error"] = "Informations not added ❌, there might be something wrong about the query : " + str(e)

    finally:
        cursor.close() # We close the message no matter what
    
    return result

def read_image_informations(image_id:int):
    """Method used to read informations about an image from the table using his id

    Args:
        image_id (int): the image id

    Returns:
        [tuple]: ({(id :int, filename :string, width :int, height :int, weight :int, format :string)},boolean)
    """
    #define connection and cursor
    connection = sqlite3.connect('Database\\thumbnailing_system.db') #connection

    cursor = connection.cursor() #cursor

    result = {} # Result Dictionnary

    queryStatus = True

    #select an image informations in the table
    try:
        # Request and execution
        query = "SELECT * FROM images WHERE id = ?"
        cursor.execute(query, (image_id,))

        # Result from request
        result["image"] = cursor.fetchone()

        # Bollean watching if the result is empty or not
        if result["image"] == None:
            queryStatus = False

    except ValueError as e:
        queryStatus = False
        result["Error"] = "Error ❌, there might be something wrong about the query : " + str(e)
        
    finally:
        cursor.close()
    
    return result, queryStatus

def delete_all():
    """Method used to delete all informations from the table

    Returns:
        [dict]: [return a dictionnary with an message if everything is okay, error message otherwhise]
    """
    #define connection and cursor
    connection = sqlite3.connect('Database\\thumbnailing_system.db') #connection

    cursor = connection.cursor() #cursor
    result = {}

    directories = []
    directories.append(Path('./image/thumbnails'))
    directories.append(Path('./image/loaded'))
    
    #delete all images informations in the table
    try:

        for i in range(len(directories)):
            utilities.clean_directory(directories[i])

        cursor.execute("DROP TABLE images")
        result["Message"] = "Your table is clean, all line deleted ✔️"

        connection.commit() # Save (commit) the changes
        
    except sqlite3.OperationalError as e:
        result["Error"] = "Error ❌, there might be something wrong : " + str(e)

    finally:
        cursor.close()
    
    return result

def read_all_images():
    """Method used to read all from the table

    Returns:
        [(list of tuple)]: [(list of tuple)]: (id :int, width :int, height :int, weight :int, format :string)
    """
    
    #define connection and cursor
    connection = sqlite3.connect('Database\\thumbnailing_system.db') #connection
    
    cursor = connection.cursor() #cursor
    result = {}

    #show all images informations in the table
    try:
        cursor.execute("SELECT * FROM images ORDER BY id ASC")
        result["images"] = cursor.fetchall()

    except sqlite3.OperationalError as e:
        result["Error"] = "Error ❌, there might be something wrong : " + str(e)

    finally:
        cursor.close()
    
    return result
    