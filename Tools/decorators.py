from flask import escape

# Decorateur pour tester les parametres des fonctions 
def parameters_accepted(*types):
    """Decorator used to test functions's paramaters type

    Args:
        list of type of parameters by position. Eg : @parameters_accepted(int, (int,float))

    Returns:
        AssertionError if parameters type don't match
    """
    def check_parameters(f):
        assert len(types) == f.__code__.co_argcount

        def new_f(*args, **kwds):
            for (a, t) in zip(args, types):
                assert isinstance(a, t), \
                       "arg %r does not match %s" % (a, t)
            return f(*args, **kwds)

        new_f.__name__ = f.__name__
        return new_f
    return check_parameters

# func(3, 2) # -> 6
# func('3', 2) # -> AssertionError: arg '3' does not match <type 'int'>